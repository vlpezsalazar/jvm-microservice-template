import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("plugin.allopen")
    kotlin("kapt")
    id("application")
    id("com.github.johnrengelman.shadow") version "5.2.0"
}


group = "victorlsgr"

version = "0.0.0-SNAPSHOT"


repositories {
    mavenCentral()
}

sourceSets {
    create("blackBoxTest")
}


dependencies {
    implementation(dependency.kotlinStandardLib)
    implementation(platform(dependency.micronautBom))
    implementation(dependency.micronaut)

    kapt(platform(dependency.micronautBom))
    kapt(dependency.micronautInject)
    kapt(dependency.micronautValidation)

    kaptTest(platform(dependency.micronautBom))
    kaptTest(dependency.micronautInject)

    runtimeOnly(dependency.kotlinJackson)
    runtimeOnly(dependency.logback)
    runtimeOnly(dependency.zipkin)


    testImplementation(platform(dependency.micronautBom))
    testImplementation(dependency.micronautTest)

    blackBoxTestImplementation(dependency.kotlinStandardLib)
    blackBoxTestImplementation(dependency.cucumber)
    blackBoxTestImplementation(dependency.blackBoxTesting)
}

task<Test>("blackBoxTest") {
    description = "Runs the black box tests"
    group = "verification"
    testClassesDirs = sourceSets["blackBoxTest"].output.classesDirs
    classpath = sourceSets["blackBoxTest"].runtimeClasspath
    mustRunAfter(tasks["test"])
}


tasks.withType<Test> {
    useJUnitPlatform()
}

application {
    mainClassName = "victorlsgr.Application"

    applicationDefaultJvmArgs = listOf(
        "-noverify",
        "-XX:TieredStopAtLevel=1",
        "-Dcom.sun.management.jmxremote"
    )

}

allOpen {
    annotation("io.micronaut.aop.Around")
    annotation("io.micronaut.http.annotation.Controller")
}

tasks.withType<com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar> {
    mergeServiceFiles()
    
}

tasks.withType<KotlinCompile> {
    kotlinOptions.let {
        it.jvmTarget = "11"
        it.javaParameters = true
    }
}
