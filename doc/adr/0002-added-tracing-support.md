# 2. Added Tracing Support

Date: 2020-10-20

## Status

Accepted

## Context

On a microservice architecture comprising different domains and several 10s of different services knowing/following the 
flow of a request can be a daunting task, so having the ability to trace them is a crucial part to enhance the observability
and understandability of any distributed system. 

## Decision

Added support for tracing requests coming to controllers. Created an acceptance test featuring [zipkin](https://zipkin.io/)
to show a tool that receives and displays tracing information. The test's client is also instrumented and send traces to this
tool.

## Consequences

+ Enhanced ability to know from where a request is coming to a microservice and easiness to pinpoint performance problems
if they would ever become present.

+ Capacity to visualize the flows followed on a particular scenario, if the same pattern instrumenting acceptance/E2E clients
in the test is followed. That boost the ability to understand the system as a whole for new members of the team.

+ When a complex scenario involves several steps and calls to different services, it's easy to follow the flows and to 
  know where, i.e. in which sequence of microservices collaborating in a posed scenario, a failure has happened.


- Sending traces poses additional overload on the computation and networking layers and they should be sent on a low rate
 (controlled by the ZIPKIN_RATE env var) in production on those system supporting high traffic.
