package victorlsgr.hello

import io.kotlintest.shouldBe
import io.kotlintest.specs.BehaviorSpec
import io.micronaut.context.annotation.Primary
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest
import io.micronaut.test.annotation.MockBean
import io.micronaut.test.extensions.kotlintest.MicronautKotlinTestExtension.getMock
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import io.reactivex.Maybe

@MicronautTest(propertySources = ["classpath:test.yaml"])
class ProxyHelloApiTest(
    private val client: ProxyHelloApi.HelloWorldClient,
    @Client("/") private val helloClient: HttpClient
) : BehaviorSpec({

    Given("proxy should target a hello resource when a request is sent") {
        val response = "hello World from Mock!"

        When("A request to proxy is made") {
            val helloClientMock = getMock(client)
            every { helloClientMock.hello() } answers { Maybe.just(response) }

            val result = helloClient.toBlocking().retrieve("/proxy", String::class.java)

            Then("hellod returns the response received by the client") {
                verify { helloClientMock.hello() }
                result shouldBe response
            }
        }
    }

}) {

    @Primary @MockBean
    fun client() : ProxyHelloApi.HelloWorldClient = mockk()

}