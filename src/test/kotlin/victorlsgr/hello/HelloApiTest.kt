package victorlsgr.hello

import io.kotlintest.shouldBe
import io.kotlintest.specs.StringSpec
import io.micronaut.http.client.HttpClient
import io.micronaut.http.client.annotation.Client
import io.micronaut.test.annotation.MicronautTest

@MicronautTest(propertySources = ["classpath:test.yaml"])
class HelloApiTest(@Client("/") private val client: HttpClient) : StringSpec({

    "hello should return 'Hello World!' when a request is sent to /hello" {
        val result = client.toBlocking().retrieve("/hello", String::class.java)
        result shouldBe "Hello World!"
    }

})