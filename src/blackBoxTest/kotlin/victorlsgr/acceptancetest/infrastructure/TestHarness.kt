package victorlsgr.acceptancetest.infrastructure

import brave.ScopedSpan
import brave.Span
import brave.Tracer
import brave.Tracing
import brave.http.HttpClientHandler
import brave.http.HttpClientRequest
import brave.http.HttpClientResponse
import brave.http.HttpTracing
import brave.propagation.TraceContext
import io.cucumber.core.logging.Logger
import io.cucumber.core.logging.LoggerFactory
import io.cucumber.java8.Scenario
import io.restassured.RestAssured
import io.restassured.builder.RequestSpecBuilder
import io.restassured.filter.Filter
import io.restassured.filter.FilterContext
import io.restassured.response.Response
import io.restassured.specification.FilterableRequestSpecification
import io.restassured.specification.FilterableResponseSpecification
import victorlsgr.acceptancetest.HelloAT.Companion.ServiceSpec
import victorlsgr.acceptancetest.HelloAT.Companion.ServiceSpec.Companion.ZipkinService
import zipkin2.reporter.AsyncReporter
import zipkin2.reporter.okhttp3.OkHttpSender


internal object RestAssuredConfiguration {

    private val log: Logger = LoggerFactory.getLogger(RestAssuredConfiguration::class.java)
    internal val LOGGING_FILTER = Filter { requestSpec: FilterableRequestSpecification,
                                           responseSpec: FilterableResponseSpecification,
                                           ctx: FilterContext ->
        ctx.run {
            log.trace { "${requestSpec.method} ${requestSpec.uri}" }
            next(requestSpec, responseSpec)
        }.apply {
            log.trace { statusLine() }
        }
    }

    internal class TracingFilter(tracing: Tracing) : Filter {
        private val tracer: Tracer = tracing.tracer()
        private val handler: HttpClientHandler<HttpClientRequest, HttpClientResponse> = HttpClientHandler.create(
            HttpTracing.create(tracing)
        )
        private val injector: TraceContext.Injector<FilterableRequestSpecification> = tracing
            .propagation()
            .injector { obj: FilterableRequestSpecification, headerName: String, newValue: String ->
                obj.replaceHeader(
                    headerName,
                    newValue
                )
            }

        override fun filter(
            requestSpec: FilterableRequestSpecification,
            responseSpec: FilterableResponseSpecification,
            ctx: FilterContext
        ): Response {
            var response: Response? = null
            var error: Throwable? = null

            val span: Span = handler.handleSend(FilterableRequestSpecificationToHttpClientRequest(requestSpec))
            try {
                response = tracer.withSpanInScope(span).use {
                    injector.inject(span.context(), requestSpec)
                    ctx.next(requestSpec, responseSpec)
                }
                return response
            } catch (e: RuntimeException) {
                error = e
                throw e
            } finally {
                handler.handleReceive(FilterableResponseSpecificationToHttpClientReponse(response), error, span)
            }
        }
    }

    internal class FilterableRequestSpecificationToHttpClientRequest(val request: FilterableRequestSpecification) :
        HttpClientRequest() {
        override fun path() = request.basePath

        override fun header(name: String?, value: String?) {
            request.header(name, value)

        }

        override fun header(name: String?): String? = request.headers[name]?.value

        override fun url(): String = request.uri

        override fun method(): String = request.method

        override fun unwrap(): Any = request

    }

    internal class FilterableResponseSpecificationToHttpClientReponse(val response: Response?) :
        HttpClientResponse() {
        override fun statusCode(): Int = response?.statusCode ?: throw IllegalStateException()

        override fun unwrap(): Any? = response
    }

}


data class TracingInfo(internal val tracing: Tracing, internal val span: ScopedSpan) {

    fun traceId(): String {
        return span.context().traceIdString()
    }

    fun closeResources() {
        span.finish()
        tracing.close()
    }


}


class TracingInfoBuilder {
    var serviceSpec: ServiceSpec? = null
    var scenario: Scenario? = null

    fun build(): TracingInfo {
        val tracing = Tracing.newBuilder()
            .localServiceName(
                serviceSpec
                    ?.toString()
                    ?: throw java.lang.IllegalStateException("Can not build TracingInfo without a client")
            ).spanReporter(
                AsyncReporter.create(
                    OkHttpSender.create(ZipkinService.uri("api/v2/spans").toString())
                )
            ).build()
        val span: ScopedSpan = tracing.tracer().startScopedSpan(
            scenario
                ?.let { "Scenario: ${it.name}" }
                ?: ""
        )
        return TracingInfo(tracing, span)
    }
}

fun RequestSpecBuilder.addTracingInfo(tracing: TracingInfo): RequestSpecBuilder {

    RestAssured.enableLoggingOfRequestAndResponseIfValidationFails()

    return this
        .addFilter(RestAssuredConfiguration.TracingFilter(tracing.tracing))
        .addFilter(RestAssuredConfiguration.LOGGING_FILTER)


}

