package victorlsgr.acceptancetest

import io.cucumber.java8.En
import io.cucumber.junit.Cucumber
import io.cucumber.junit.CucumberOptions
import io.restassured.RestAssured
import io.restassured.builder.RequestSpecBuilder
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.matcher.AssertionMatcher
import org.awaitility.Awaitility.await
import org.junit.runner.RunWith
import org.testcontainers.containers.DockerComposeContainer
import victorlsgr.acceptancetest.HelloAT.Companion.ServiceSpec.Companion.HelloService
import victorlsgr.acceptancetest.HelloAT.Companion.ServiceSpec.Companion.ProxyService
import victorlsgr.acceptancetest.HelloAT.Companion.ServiceSpec.Companion.ZipkinService
import victorlsgr.acceptancetest.infrastructure.TracingInfo
import java.io.File
import java.net.URI
import java.util.concurrent.TimeUnit

@RunWith(Cucumber::class)
@CucumberOptions(
    features = ["src/blackBoxTest/resources/features"],
    plugin = [
        "pretty",
        "junit:build/reports/tests/acceptance-test/junit.xml",
        "html:build/reports/tests/acceptance-test/"
    ],
    glue = ["victorlsgr.acceptancetest"],
    strict = true
)
class HelloAT : En {

    interface Configurable {
        fun configure(): Any = this
    }

    companion object {
        internal val env = KDockerComposeContainer(
            "src/blackBoxTest/resources/docker-compose.yml"
        )
            .withLocalCompose(true)
            .withBuild(true)
            .withRemoveImages(DockerComposeContainer.RemoveImages.LOCAL)
            .withExposedService(ZipkinService.service, ZipkinService.port)
            .withExposedService(HelloService.service, HelloService.port)
            .withExposedService(ProxyService.service, ProxyService.port)


        sealed class ServiceSpec(val service: String, val port: Int) : En, Configurable {
            companion object {
                object HelloService : ServiceSpec("hello", 8080)
                object ProxyService : ServiceSpec("proxy", 8080)
                object ZipkinService : ServiceSpec("zipkin", 9411) {
                    fun validateTrace(tracingInfo: TracingInfo, from: String, kind: String, responseAssertion: (toAssert: Map<String, *>) -> Unit = {}) {
                        assertThat(tracingInfo).hasNoNullFieldsOrProperties()

                        val traceId = tracingInfo.traceId()

                        await()
                            .timeout(3, TimeUnit.SECONDS)
                            .untilAsserted {
                                RestAssured.given(
                                    RequestSpecBuilder().setBaseUri(
                                        url()
                                    ).build()
                                ).get("api/v2/trace/$traceId")
                                    .then().assertThat()
                                    .statusCode(200)
                                    .and().body("") {
                                        object : AssertionMatcher<List<Map<String, *>>>() {
                                            override fun assertion(actual: List<Map<String, *>>) {
                                                assertThat(actual).anySatisfy {
                                                    assertThat(it).containsEntry("traceId", traceId)
                                                    assertThat(it).containsEntry("kind", kind)
                                                    assertThat(it).extractingByKey("localEndpoint")
                                                        .hasFieldOrPropertyWithValue("serviceName", from)
                                                   responseAssertion(it)
                                                }
                                            }
                                        }
                                    }
                            }

                    }

                }

                val clients = mapOf(
                    Pair("Hello", HelloService),
                    Pair("Proxy", ProxyService)
                )
            }

            fun url() =
                "http://${env.getServiceHost(this.service, this.port)}:${env.getServicePort(this.service, this.port)}/"

            fun uri(path: String = ""): URI = URI(url()).resolve(path)

            override fun toString(): String = "client of $service"

        }
    }

    internal class KDockerComposeContainer(dockerComposeFilePath: String) :
        DockerComposeContainer<KDockerComposeContainer>(
            File(dockerComposeFilePath)
        )

    init {
        env.start()

    }

}