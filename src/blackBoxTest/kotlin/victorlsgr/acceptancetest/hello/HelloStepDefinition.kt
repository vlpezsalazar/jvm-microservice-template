package victorlsgr.acceptancetest.hello

import io.cucumber.java8.En
import io.cucumber.java8.Scenario
import io.restassured.RestAssured
import io.restassured.builder.RequestSpecBuilder
import io.restassured.module.kotlin.extensions.Then
import io.restassured.response.Response
import io.restassured.specification.RequestSpecification
import org.assertj.core.api.Assertions
import org.awaitility.Awaitility
import org.awaitility.Durations
import org.hamcrest.CoreMatchers
import victorlsgr.acceptancetest.HelloAT.Companion.ServiceSpec
import victorlsgr.acceptancetest.HelloAT.Companion.ServiceSpec.Companion.ZipkinService
import victorlsgr.acceptancetest.HelloAT.Companion.ServiceSpec.Companion.clients
import victorlsgr.acceptancetest.infrastructure.TracingInfo
import victorlsgr.acceptancetest.infrastructure.TracingInfoBuilder
import victorlsgr.acceptancetest.infrastructure.addTracingInfo

class HelloStepDefinition : En {
    private var tracingInfo: TracingInfo? = null
    private lateinit var response: Response
    private lateinit var client: RequestSpecification
    private lateinit var tracingInfoBuilder: TracingInfoBuilder

    init {

        Given("Configured service is healthy") {
            Awaitility.await().atMost(Durations.FIVE_SECONDS).until {
                client.get("/health").statusCode() == 200
            }
        }

        When("{string} is called") { path: String ->
            response = client.get(path)
        }

        Then("{string} is returned") { word: String ->
            response.Then {
                statusCode(200)
                body(CoreMatchers.equalTo(word))
            }
        }

        Given("{clientSpec} is configured") { serviceSpec: ServiceSpec ->
            client = RestAssured.given(
                RequestSpecBuilder()
                    .setBaseUri(
                        serviceSpec.url()
                    ).build()
            )
        }

        Given("{clientSpec} is instrumented with tracing information") { serviceType: ServiceSpec ->
            tracingInfo = tracingInfoBuilder
                .apply { this.serviceSpec = serviceType }
                .build()
                .apply {
                    client = RestAssured.given(
                        RequestSpecBuilder()
                            .setBaseUri(
                                serviceType.url()
                            ).addTracingInfo(this)
                            .build()
                    )
                }
        }


        Then("Zipkin receives a trace of the request from {string} to {string}") { from: String, to: String->
            ZipkinService.validateTrace(tracingInfo!!, from, "CLIENT"){
                Assertions.assertThat(it).extractingByKey("remoteEndpoint")
                    .hasFieldOrPropertyWithValue("serviceName", to)
            }
        }

        Then("Zipkin receives a trace of the request received by the {string} service") { to: String->
            ZipkinService.validateTrace(tracingInfo!!,  to, "SERVER")
        }



        ParameterType("clientSpec", "([a-zA-Z]+)\\s+client") { clientName: String -> clients[clientName] }

        Before { scenario: Scenario ->
            tracingInfoBuilder = TracingInfoBuilder().apply {
                this.scenario = scenario
            }
        }

        After { _ : Scenario ->
            tracingInfo?.closeResources()
        }


    }

}
