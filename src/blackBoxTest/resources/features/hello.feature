# Created by victor at 2020-02-19
Feature: hello
  The service should run correctly

  Scenario: Hello World!
    Given Hello client is configured
    And Configured service is healthy
    When '/hello' is called
    Then 'Hello World!' is returned
