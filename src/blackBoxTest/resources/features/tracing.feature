# Created by victor at 2020-03-01
Feature: Tracing
  Whenever a request is sent to hello,
  It will add or preserve the tracing information of the request/response
  So Zipkin will be able to trace it.

  Scenario: Clients calls are also traced
    Given Proxy client is instrumented with tracing information
    When '/proxy' is called
    Then Zipkin receives a trace of the request from 'proxy' to 'hello-world'
    And Zipkin receives a trace of the request received by the 'hello-world' service
    And 'Hello World!' is returned
