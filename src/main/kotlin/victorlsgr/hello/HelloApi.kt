package victorlsgr.hello

import io.micronaut.http.MediaType
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller("/hello")
class HelloApi {

    @Get(produces = [MediaType.TEXT_PLAIN])
    fun world(): String = "Hello World!"

}