package victorlsgr.hello

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.client.annotation.Client
import io.reactivex.Maybe


@Controller("/proxy")
class ProxyHelloApi(val client: HelloWorldClient) {

    @Get
    fun proxyHelloTo(): Maybe<String> {
        return client.hello()
    }

    @Client("hello-world")
    interface HelloWorldClient {
        @Get
        fun hello(): Maybe<String>
    }
}