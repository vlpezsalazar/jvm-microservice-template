FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim

COPY build/libs/jvm-microservice-template-*-all.jar jvm-microservice-template.jar

EXPOSE 8080

ENTRYPOINT exec java -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar jvm-microservice-template.jar