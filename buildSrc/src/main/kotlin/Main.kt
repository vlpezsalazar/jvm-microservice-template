import org.gradle.api.artifacts.dsl.DependencyHandler

//support for arrays of dependencies, same as gradle has.

fun DependencyHandler.implementation(dependencyNotations: Collection<Any>) =
    dependencyNotations.iterator().forEachRemaining { add("implementation", it) }

fun DependencyHandler.testImplementation(dependencyNotations: Collection<Any>) =
    dependencyNotations.iterator().forEachRemaining { add("testImplementation", it) }

fun DependencyHandler.blackBoxTestImplementation(dependencyNotations: Collection<Any>) =
    dependencyNotations.iterator().forEachRemaining { add("blackBoxTestImplementation", it) }

fun DependencyHandler.blackBoxTestImplementation(dependencyNotations: Any) =
    add("blackBoxTestImplementation", dependencyNotations)

fun DependencyHandler.runtimeOnly(dependencyNotations: Collection<Any>) =
    dependencyNotations.iterator().forEachRemaining { add("runtimeOnly", it) }