/**
 * As a rule of thumb, place here a dependency when:
 * <ul>
 *     <li> You want IDE support for it.
 *     <li> It's shared among different modules/sourceSets and you want to keep the version consistently.
 * </ul>
 *
 */
package dependency

import dependency.Version.cucumberVersion
import dependency.Version.micronautVersion
import dependency.Version.restAssuredVersion

/**
 * Versions of dependencies.
 */
object Version {
    const val micronautVersion = "1.3.5"
    const val cucumberVersion = "5.3.0"
    const val restAssuredVersion = "4.2.0"
}


// DEFINE YOUR DEPENDENCIES HERE

val kotlinStandardLib = setOf(
    "org.jetbrains.kotlin:kotlin-stdlib-jdk8",
    "org.jetbrains.kotlin:kotlin-reflect"
)

const val micronautBom = "io.micronaut:micronaut-bom:${micronautVersion}"

val micronaut = setOf(
    "io.micronaut:micronaut-runtime",
    "javax.annotation:javax.annotation-api",
    "io.micronaut:micronaut-http-server-netty",
    "io.micronaut:micronaut-http-client",
    "io.micronaut:micronaut-management",
    "io.micronaut:micronaut-tracing",
    "io.opentracing.brave:brave-opentracing"
)

const val micronautGraal = "io.micronaut:micronaut-graal"
const val nativeImageSvm = "org.graalvm.nativeimage:svm"

val zipkin = setOf(
    "io.zipkin.brave:brave-instrumentation-http",
    "io.zipkin.reporter2:zipkin-reporter"
)

const val micronautInject = "io.micronaut:micronaut-inject-java"
val micronautValidation = "io.micronaut:micronaut-validation"

const val kotlinJackson = "com.fasterxml.jackson.module:jackson-module-kotlin:2.9.8"
const val logback = "ch.qos.logback:logback-classic:1.2.3"

val micronautTest = setOf(
    "io.micronaut.test:micronaut-test-kotlintest",
    "io.mockk:mockk:1.9.3",
    "io.kotlintest:kotlintest-runner-junit5:3.3.2"
)

val cucumber = setOf(
    "io.cucumber:cucumber-java8:$cucumberVersion",
    "io.cucumber:cucumber-junit:$cucumberVersion",
    "io.cucumber:cucumber-picocontainer:$cucumberVersion",
    "org.junit.vintage:junit-vintage-engine:4.12.0",
    logback
)

val zipkinReports =  setOf(
    "io.zipkin.brave:brave-instrumentation-http:5.10.1",
    "io.zipkin.reporter2:zipkin-sender-okhttp3:2.12.1"
)

const val testcontainers = "org.testcontainers:testcontainers:1.15.0-rc2"

val restassured = setOf(
    "io.rest-assured:rest-assured-all:$restAssuredVersion",
    "io.rest-assured:kotlin-extensions:$restAssuredVersion"
)
const val wiremock = "com.github.tomakehurst:wiremock-jre8:2.26.0"

const val awaitility = "org.awaitility:awaitility:4.0.2"

const val assertJ = "org.assertj:assertj-core:3.16.1"

val blackBoxTesting = restassured +
        wiremock +
        awaitility +
        testcontainers +
        assertJ +
        zipkinReports
